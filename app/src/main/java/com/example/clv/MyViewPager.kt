package com.example.clv

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter


class MyViewPager(fragmentActivity:FragmentActivity, private val fragment: List<Fragment>) :FragmentStateAdapter(fragmentActivity){
    override fun getItemCount(): Int {
        return fragment.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragment[position]
    }


}