package com.example.clv

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView

class Adapter(private val context: Context, public var musicDataList: Array<MusicData>) : BaseAdapter() {
    companion object {
        private var inflater: LayoutInflater? = null
    }
    val music = MusicGestion()
    init {
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getCount(): Int {
        return musicDataList.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var cv =convertView
        if (cv== null){
            cv = inflater!!.inflate(R.layout.cardview_slide_panel,parent,false)
        }
        val holder = initHolder(cv!!)
        holder.editor.text = musicDataList[position].editor
        holder.title.text = musicDataList[position].song_name



        cv.setOnClickListener{
            // start song and at end of page make a start/stop button and name of the song played and a delete button that destruct that new view
            music.playStop((musicDataList[position].path))
        }

        return cv
    }


    class Holder {
        lateinit var title: TextView
        lateinit var editor: TextView

    }
    private fun initHolder(view: View): Holder {
        val holder = Holder()
        holder.title = view.findViewById(R.id.title)
        holder.editor = view.findViewById(R.id.editor)
        holder.title.maxLines = 1
        holder.title.isSelected = true
        holder.title.isSingleLine = true
        holder.title.isFocusable = true
        holder.title.isFocusableInTouchMode = true
        holder.editor.maxLines = 1
        holder.editor.isSelected = true
        holder.editor.isSingleLine = true
        holder.editor.isFocusable = true
        holder.editor.isFocusableInTouchMode = true
        return holder
    }
}