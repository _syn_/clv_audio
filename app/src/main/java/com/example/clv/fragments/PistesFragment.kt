package com.example.clv.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.clv.R
import android.widget.ListView
import android.widget.BaseAdapter
import com.example.clv.Adapter
import android.content.Context
import com.example.clv.MainActivity
import com.example.clv.MusicData

class PistesFragment : Fragment(){



    private lateinit var v: View



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        v = inflater.inflate(R.layout.fragment_pistes, container, false)
        return v
    }

    fun refresh(MusicDataList: Array<MusicData>){
        val listView = v.findViewById<ListView>(R.id.list_panel)

        if (listView.adapter == null){
            val listAdapter = Adapter(requireContext(), MusicDataList)
            listView.adapter = listAdapter
        }
        else{
            (listView.adapter as Adapter).musicDataList = MusicDataList
            (listView.adapter as Adapter).notifyDataSetChanged()
        }

    }
}



/*
Depuis la class Adapter, tu change musicDataList en public et val au lieu de var, puis ;  ok

Dans Pistes Fragment, dans la fonction refresh :

Vérifie que listview.adapter est != null. S'il est nul, tu crées ton instance Adapter(requireContext...) comme c'est déjà le cas  ok

sinon tu utilise l'instance Adapter existante (présente dans listView.adapter), tu met à jour le contenu de MusicDataList

Et tu rafraichis l'adapter sans un créer un nouveau (listView.adapter as Adapter).notifyDataSetChanged().
Dans ce cas, une seule et unique instance de MusicGestion sera créer par ta seule et unique instance de ton adapter.
 */