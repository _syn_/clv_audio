package com.example.clv

import android.Manifest
import android.content.pm.PackageManager
import android.content.res.Resources
import android.media.MediaMetadataRetriever
import android.media.MediaMetadataRetriever.*
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.clv.fragments.EqualFragment
import com.example.clv.fragments.ParamFragment
import com.example.clv.fragments.PistesFragment
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import java.io.File


class MainActivity : AppCompatActivity() {
    private lateinit var tabLayout: TabLayout
    private  lateinit var viewPager: ViewPager2
    private lateinit var fragments: List<Fragment>
    private val musicDataList = mutableListOf<MusicData>()

   // public  songList = []  // contain n element of type "song"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tabLayout = findViewById(R.id.tablayout)
        viewPager = findViewById(R.id.view_pager)

       fragments = listOf(PistesFragment(), EqualFragment(), ParamFragment())

        viewPager.adapter = MyViewPager(this, fragments)
        TabLayoutMediator(tabLayout, viewPager){ tab, index ->
        tab.text = when(index){
            0->{"pistes"}
            1->{"equal"}
            2->{"param"}
            else -> {throw Resources.NotFoundException("Position Not Found") }
        }
        }.attach()

        // partie scan
        findViewById<Button>(R.id.scan).setOnClickListener {
            musicDataList.clear()  // more easy than check when scan if song scanned is already in list
//            val pathList= mutableListOf<String?>()
//            val songList = mutableListOf<String?>()
//            val editorList= mutableListOf<String?>()

            val list = load2()
            if (list != null) {
                for (f: File in list){
                    val mmr = MediaMetadataRetriever().also {
                        it.setDataSource(f.absolutePath)
                    }
                    if (mmr.extractMetadata(METADATA_KEY_TITLE).isNullOrBlank()){
                        val song = MusicData( f.absolutePath,f.name,mmr.extractMetadata(
                            METADATA_KEY_ARTIST
                        ))
                        musicDataList.add(song)
                    }
                    else{
                        val song = MusicData(f.absolutePath,mmr.extractMetadata(METADATA_KEY_TITLE),mmr.extractMetadata(
                            METADATA_KEY_ARTIST
                        ))
                        musicDataList.add(song)
                    }
                    /*editorList.add(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST))
                    pathList.add(f.absolutePath)
                    songList.add(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE))*/
                }
            }
            (fragments[0] as PistesFragment).refresh(musicDataList.toTypedArray())
        }

       findViewById<SearchView>(R.id.search).setOnQueryTextListener(object : SearchView.OnQueryTextListener{
           override fun onQueryTextSubmit(p0: String?): Boolean {return true}


           override fun onQueryTextChange(query: String?): Boolean {
               if (query==null){return false}

               (fragments[0] as PistesFragment).refresh(musicDataList.filter { musicData -> musicData.song_name?.lowercase()?.contains(query.lowercase())==true || musicData.editor?.lowercase()?.contains(query) == true }.toTypedArray())

               return true
           }
       })

    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) {
                    if ((ContextCompat.checkSelfPermission(this@MainActivity,
                            Manifest.permission.READ_EXTERNAL_STORAGE) ===
                                PackageManager.PERMISSION_GRANTED)) {
                        Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }
    override fun onResume() {
        super.onResume()
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this@MainActivity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)
            } else {
                ActivityCompat.requestPermissions(this@MainActivity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)
            }
        }
        //Toast.makeText(this, "voila", Toast.LENGTH_SHORT).show()
    }
    private fun load2(): Array<File>?{
        var dir = File(Environment.getExternalStorageDirectory(), "Music")
        if (dir.exists() && dir.isDirectory) {
            return dir.listFiles { _, name -> name.contains(".mp3") || name.contains(".wav") }
        }
        return null
    }
}



