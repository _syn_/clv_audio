package com.example.clv
import android.media.MediaPlayer
import android.media.audiofx.Equalizer

class MusicGestion {


    private var player = MediaPlayer()
    private var path = ""
    private var length = 0
    val equal = Equalizer(1,player!!.audioSessionId)

      init {
      }

    fun playStop(Path: String) {

        if (path == "") {
            player = MediaPlayer()
            player.setDataSource(Path)
            player.prepare()
            player.start()
            path = Path
            player.setOnCompletionListener { player.release() }

        } else if (player.isPlaying && path != Path) {
            player.release()
            player = MediaPlayer()
            player.setDataSource(Path)
            player.prepare()
            player.start()
            path = Path
            player.setOnCompletionListener { player.release() }
        } else if (path != Path) {
            //player.stop()
            player.release()
            player = MediaPlayer()
            player.setDataSource(Path)
            player.prepare()
            player.start()
            path = Path
            player.setOnCompletionListener { player.release() }

        } else if (player.isPlaying) {
            player.pause()
            length = player.currentPosition
        } else if (path == Path) {
            player.seekTo(length)
            player.start()
            player.setOnCompletionListener { player.release() }

        } else {
            player.release()
        }



    }
    fun eq(){
        equal.enabled = true
        equal.numberOfBands
        equal.numberOfPresets

    }

}


/* try {
     val EqIntent = Intent(AudioEffect.ACTION_DISPLAY_AUDIO_EFFECT_CONTROL_PANEL)
     EqIntent.putExtra(AudioEffect.EXTRA_AUDIO_SESSION, player!!.audioSessionId)
     EqIntent.putExtra(AudioEffect.EXTRA_PACKAGE_NAME, PACKAGE_NAME)
     EqIntent.putExtra(AudioEffect.EXTRA_CONTENT_TYPE, AudioEffect.CONTENT_TYPE_MUSIC)
     this.sendBroadcast(EqIntent)
 }
 catch (e:Exception){}*/